import React from 'react';

import './Logo.css';
import menuLogo from '../../assets/images/logo.jpg';

const Logo = () => (
  <div className="Logo">
    <img src={menuLogo} alt="MyLogo" />
  </div>
);


export default Logo;