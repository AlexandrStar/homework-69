import React, {Component} from 'react';
import {connect} from "react-redux";
// import {Redirect} from "react-router-dom";

import Button from '../../../components/UI/Button/Button';
import Spinner from '../../../components/UI/Spinner/Spinner';

// import {initIngredients} from "../../../store/actions/burgerBuilder";
// import {createOrder} from "../../../store/actions/order";

import './OrderForm.css';
import {createOrder} from "../../../store/actions/order";
import {createMenu} from "../../../store/actions/menuBulder";

class OrderForm extends Component {

  state = {
    name: '',
    email: '',
    street: '',
    postal: '',
  };

  valueChanged = event => {
    const {name, value} = event.target;
    this.setState({[name]: value});
  };

  orderHandler = event => {
    event.preventDefault();

    const orderData = {
      products: this.props.products,
      customer: {...this.state}
    };

    this.props.createOrder(orderData, this.props.history);
  };

  render() {
    let form = (
      <form onSubmit={this.orderHandler}>
        <input className="Input" type="text" name="name" placeholder="Your Name"
               value={this.state.name} onChange={this.valueChanged}
        />
        <input className="Input" type="email" name="email" placeholder="Your Mail"
               value={this.state.email} onChange={this.valueChanged}
        />
        <input className="Input" type="text" name="street" placeholder="Street"
               value={this.state.street} onChange={this.valueChanged}
        />
        <input className="Input" type="text" name="postal" placeholder="Postal Code"
               value={this.state.postal} onChange={this.valueChanged}
        />
        <Button
          btnType="Success"
          onClick={this.props.purchaseCancel}
        >ORDER</Button>
      </form>
    );

    if (this.props.loading) {
      form = <Spinner/>;
    }
    // if (this.props.ordered) {
    //   form = <Redirect to="/" />;
    // }

    return (
      <div className="ContactData">
        <h4>Enter your Contact Data</h4>
        {form}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.ord.products,
  loading: state.ord.loading,
  ordered: state.ord.ordered,
});

const mapDispatchToProps = dispatch => ({
  createOrder: (orderData, history) => dispatch(createOrder(orderData,history)),
  createMenu: () => dispatch(createMenu()),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderForm);