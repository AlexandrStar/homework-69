import React from 'react';

import './OrderItem.css';

const OrderItem = ({products, customer}) => {

  const productsOutput = Object.keys(products).map(prodName => (
    <span key={prodName}>{prodName} ({products[prodName]})</span>
  ));

  const customerOutput = Object.keys(customer).map(prodName => (
    <span key={prodName}>{prodName} ({customer[prodName]})</span>
  ));

  return (

    <div className="OrderItem">
      <p>Products: {productsOutput}</p>
      <p>Customer: {customerOutput}</p>
    </div>
  );
};

export default OrderItem;