import React from 'react';
import Button from "../UI/Button/Button";

import './Menu.css';

const Menu = props => {
  return (
    Object.keys(props.products).map((order) => {
      const oneOrder = props.products[order];
      return (
        <div className="Menu" key={order}>
          <img src={oneOrder.image} alt="ImageProduct"/>
          <div className="Menu-name">
            <h5>{oneOrder.name}</h5>
            <p>KGS {oneOrder.price}</p>
          </div>
          <Button onClick={() => props.add(order, oneOrder.price)} btnType="Success">Add to card</Button>
        </div>
      )
    })
  );
};

export default Menu;