import React from 'react';
import Button from "../../UI/Button/Button";

import './Cart.css';

const Cart = props => {
  return (
      <div className="Cart">
        <h3>Cart</h3>
        <div className="Cart-total">
          {Object.keys(props.productsOrd).map((item, index) => {
              const oneItem = props.productsOrd[item];
              if (oneItem > 0){
                return (
                  <div className="Cart-item" key={index}>
                    <p>{props.products[item].name}</p>
                    <p>x {oneItem}</p>
                    <p>{props.products[item].price * oneItem}</p>
                    <Button onClick={() => props.remove(item, props.products[item].price)} btnType="Danger">X</Button>
                  </div>
                );
              }
            }
          )}
        </div>
        <div className="Cart-price">
          <p>Доставка: {props.deliv}</p>
          <p>Итого: {props.price > 0 ? props.price + props.deliv: 0}</p>
          {props.price > 0 ? <Button onClick={props.modalOn} btnType="Success">Place order</Button>: null}
        </div>
      </div>
);
};

export default Cart;