import React from 'react';

import NavigationItem from "./NavigationItem/NavigationItem";

import './NavigationItems.css';

const NavigationItems = () => (
  <ul className="NavigationItems">
    <NavigationItem to="/" exact>Menu Builder</NavigationItem>
    <NavigationItem to="/orders">Orders</NavigationItem>
  </ul>
);


export default NavigationItems;