import React, { Component } from 'react';
import './App.css';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router";
import MenuBuilder from "./containers/MenuBuilder/MenuBuilder";
import Orders from "./containers/Orders/Orders";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={MenuBuilder} />
          <Route path="/orders" component={Orders} />
          <Route render={() => <h1>No found!</h1>} />
        </Switch>
      </Layout>
    );
  }
}

export default App;
