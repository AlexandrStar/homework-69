import {
  ADD_PRODUCT,
  INIT_ORDER,
  ORDER_FAILURE,
  ORDER_REQUEST,
  ORDER_SUCCESS,
  REMOVE_PRODUCT
} from "../actions/actionTypes";

const INITIAL_PRODUCTS = {
  guro: 0,
  lepeshka: 0,
  manti: 0,
  plov: 0,
  shakarap: 0,
  shashlik: 0,
  tea: 0,
};

const initialState = {
  products: {...INITIAL_PRODUCTS},
  delivery: 150,
  totalPrice: 0,
  loading: false,
  error: null,
  ordered: false,
};

const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PRODUCT:
      return {
        ...state,
        products: {
          ...state.products,
          [action.productName]: state.products[action.productName] + 1
        },
        totalPrice: state.totalPrice + action.price
      };
      case REMOVE_PRODUCT:
      return {
        ...state,
        products: {
          ...state.products,
          [action.productName]: state.products[action.productName] - 1
        },
        totalPrice: state.totalPrice - action.price
      };
    case INIT_ORDER:
         return {...state, products: {...INITIAL_PRODUCTS}, totalPrice: 0, ordered: false};
      case ORDER_REQUEST:
        return {...state, loading: true};
      case ORDER_SUCCESS:
        return {...state, loading: false, ordered: true};
      case ORDER_FAILURE:
        return {...state, loading: false, error: action.error};
    default:
      return state;
  }
};

export default orderReducer;