import {MENU_REQUEST, MENU_SUCCESS} from "../actions/actionTypes";

const initialState = {
  products: {},
  loading: false,
};

const menuBuilderReducer = (state = initialState, action) => {
  switch (action.type) {
    case MENU_REQUEST:
      return {
        ...state,
        loading: true
      };
    case MENU_SUCCESS:
      return {
        ...state,
        products: action.products,
        loading: false
      };
    default:
      return state;
  }
};

export default menuBuilderReducer;