import axios from '../../axios-orders';

import {ADD_PRODUCT, INIT_ORDER, ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS, REMOVE_PRODUCT} from "./actionTypes";

export const addProduct = (productName, price) => ({type: ADD_PRODUCT, productName, price});
export const removeProduct = (productName, price) => ({type: REMOVE_PRODUCT, productName, price});

export const orderRequest = () => ({type: ORDER_REQUEST});
export const orderSuccess = () => ({type: ORDER_SUCCESS});
export const orderFailure = error => ({type: ORDER_FAILURE, error});
export const initOrder = () => ({type: INIT_ORDER});

export const createOrder = (orderData, history) => {
  return dispatch => {
    dispatch(orderRequest());

    axios.post('orders.json', orderData).then(
      response => {
        dispatch(orderSuccess());
        dispatch(initOrder());
        history.push('/');
      },
      error => dispatch(orderFailure(error))
    );
  }
};