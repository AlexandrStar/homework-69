import axios from '../../axios-orders';
import {MENU_FAILURE, MENU_REQUEST, MENU_SUCCESS} from "./actionTypes";

export const menuRequest = () => ({type: MENU_REQUEST});
export const menuSuccess = products => ({type: MENU_SUCCESS, products});
export const menuFailure = error => ({type: MENU_FAILURE, error});

export const createMenu = () => {
  return dispatch => {
    dispatch(menuRequest());

    axios.get('/products.json').then(
      response => {
        dispatch(menuSuccess(response.data));
        // history.push('/');
      },
      error => dispatch(menuFailure(error))
    );
  }
};