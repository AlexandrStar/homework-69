import React, {Component} from 'react';

import axios from '../../axios-orders';
import Spinner from "../../components/UI/Spinner/Spinner";
import OrderItem from "../../components/Menu/OrderItem/OrderItem";

class Orders extends Component {
  state = {
    orders: [],
    loading: true
  };

  async componentDidMount() {
    try {
      const response = await axios.get('/orders.json');
      if (!response.data) return;

      const orders = Object.keys(response.data).map((id) => ({
        ...response.data[id],
        id
      }));
      this.setState({orders});
    } catch (e) {

    } finally {
      this.setState({loading: false})
    }
  }

  render() {
    if (this.state.loading) {
      return <Spinner />;
    }

    return this.state.orders.map(order => (
        <OrderItem
          products={order.products}
          customer={order.customer}
        />
    ));
  }
}

export default Orders;