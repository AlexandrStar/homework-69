import React, {Component} from 'react';
import Menu from "../../components/Menu/Menu";
import {createMenu} from "../../store/actions/menuBulder";
import {connect} from "react-redux";
import Spinner from "../../components/UI/Spinner/Spinner";
import Cart from "../../components/Menu/Cart/Cart";

import './MenuBuilder.css';
import {addProduct, initOrder, removeProduct} from "../../store/actions/order";
import OrderForm from "../../components/Menu/OrderForm/OrderForm";
import Modal from "../../components/UI/Modal/Modal";

class MenuBuilder extends Component {
  state = {
    purchasing: false
  };

  purchase = () => {
    this.setState({purchasing: true});
  };

  purchaseCancel = () => {
    // this.props.initOrder();
    this.setState({purchasing: false});
  };

  purchaseContinue = () => {

    this.props.history.push('/checkout');
  };

  componentDidMount() {
    this.props.createMenu();
  }

  render() {
    return (
      <div className="Container">
        <Modal
          show={this.state.purchasing}
          close={this.purchaseCancel}
        >
          <OrderForm
            ingredients={this.props.ingredients}
            price={this.props.totalPrice}
            purchaseContinue={this.purchaseContinue}
            purchaseCancel={this.purchaseCancel}
          />
        </Modal>
        {this.props.loading ? <Spinner />  : <Menu add={this.props.addProduct} products={this.props.products} />}
        <Cart
          remove={this.props.removeProduct}
          productsOrd={this.props.productsOrd}
          products={this.props.products}
          deliv={this.props.delivery}
          price={this.props.totalPrice}
          modalOn={this.purchase}

        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.mb.products,
  loading: state.mb.loading,
  productsOrd: state.ord.products,
  delivery: state.ord.delivery,
  totalPrice: state.ord.totalPrice,
});

const mapDispatchToProps = dispatch => ({
    initOrder: () => dispatch(initOrder()),
    createMenu: () => dispatch(createMenu()),
    addProduct: (productName, price) => dispatch(addProduct(productName, price)),
    removeProduct: (productName, price) => dispatch(removeProduct(productName, price)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuBuilder);