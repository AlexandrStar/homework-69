import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://hw69-alexandrcher.firebaseio.com/'
});

export default instance;